#if 0
#include <reg52.h>
#define Stop 0
#define Left 1
#define Right 2

sbit ControlPort =P1^0;
sbit KeyLeft = P3^4;
sbit KeyRight = P3^5;
sbit KeyStop = P3^6;

unsigned char TimeOutCounter = 0, LeftOrRight = 0;

void InitiaTimer(void)
{
	TMOD = 0x10;
	TH1 = (65535 - 500)/256;
	TL1 = (65535 - 500)%256;
	EA = 1;
	ET1 = 1;
	TR1 = 1;
	
}

void ControlLeftOrRight(void)
{
	if(KeyStop == 0)
	{
		LeftOrRight = Stop;
	}
	if(KeyLeft == 0)
	{
		LeftOrRight = Left;
	}
	if(KeyRight == 0)
	{
		LeftOrRight = Right;
	}
}

void main(void)
{
	InitiaTimer();
	for(;;)
	{
		ControlLeftOrRight();
	}
}

void Timer1(void) interrupt 3
{
	TH1 = (65535 - 500)/256;
	TL1 = (65535 - 500)%256;
	TimeOutCounter++;
	
	switch(LeftOrRight)
	{
		case 0 :
		{
			if(TimeOutCounter <= 6)
				{
					ControlPort = 1;
				}
			else
			{
				ControlPort = 0;
			}
		break;
		}
		case 1 :
		{
			if(TimeOutCounter <= 2)
				{
					ControlPort = 1;
				}
			else
			{
				ControlPort = 0;
			}
		break;
		}
		case 2 :
		{
			if(TimeOutCounter <= 10)
				{
					ControlPort = 1;
				}
			else
			{
				ControlPort = 0;
			}
		break;
		}
		default:break;
	}
	if(TimeOutCounter == 80)
	{
		TimeOutCounter = 0;
	}
	
}